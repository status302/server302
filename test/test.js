// Mocha test, run with 'mocha' or simply 'npm test'

var Server302 = require('../lib/server302.js'),
    http = require('http'),
    assert = require("assert");
    
var server = new Server302('./fixture/');

var instance;

describe('Tests', function() {

    // Note: When we want to run the test synchronously, we need to make use of the done function & callback
    
    // We need to strat the server before testing
    before(function(done) {
        instance = http.createServer(function(request, response) {
            server.serve(request, response);
        }).listen(8100);
        instance.on("listening", function() {
            // It's only reday to run the tests once the server is actually started (listening)
            done();
        });
    });

    // This will shutdown the server after tests are performed
    after(function(done) {
        instance.close();
        done();
    });

    // Some tests
    
    it("Should fetch test.html", function(done) {
        var body = "";
        http.get({host: "localhost", port:8100, path: "/"}, function(res) {
            // Note: We need to lisren to data events to get the  (chunked) body
            res.on('data', function(chunk) {
                // Note: it might be chunked, so need to read the whole thing.
                body += chunk;
            });
            // waiting for 'end' event (end of body) before checking body content
            res.on('end', function() {
                assert.equal(res.statusCode, 200);
                assert.ok(body.toString().indexOf("<a href='/dummy.txt'>") !== -1);
                done();
            });
        })
    });
    
    it("Should fetch dummy.txt", function(done) {
        http.get({host: "localhost", port:8100, path: "/dummy.txt"}, function(res) {
            res.on('data', function(body) {
                assert.equal(res.statusCode, 200);
                assert.ok(body.toString().indexOf("test") === 0);
                done();
            });
        });
    });
    
    it("Should get 404", function(done) {
        http.get({host: "localhost", port:8100, path: "/qwerty"}, function(res) {
            assert.equal(res.statusCode, 404);
            done();
        });
    });
    
});