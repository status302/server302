// TODO: cache and server file (using 302cache) by normalized path
// TODO: option to specify browser cache setting (by mime? ) ->  "text/html":60, "*":30

var path = require('path'),
    fs = require('fs'),
    mime = require('mime');

/**
 * If log is not null, expects a winston instance, otherwise will just log to the console
 * if cache is not null, expects a cache302 instance to be used for in memory caching
 */
var Server302 = module.exports = function(root, config) {
        this.root = path.normalize(root)
        var conf = config || {}
        this.cache = conf.cache;
        this.log = conf.log || console;
        this.index = conf.index !== false;
    }

    /**
     *  Server the passed request
     */
    Server302.prototype.serve = function(req, res) {

        // We only support GET
        if (req.method !== 'GET') {
            res.writeHead(405);
            res.end('Unsupported request method (only GET supported)', 'utf8');
            return;
        }

        var filePath = path.normalize(this.root + req.url);

        this.log.info("Request for: " + filePath);

        // "Security check" on the path to make sure we are not fetching paths outside of the root
        if (filePath.indexOf(this.root) !== 0) {
            res.writeHead(403);
            res.end('Forbidden path: ' + req.url, 'utf8');
            return;
        }

        // Serve the file
        fs.stat(filePath, function serveFile(err, stats) {

            // error
            if (err) {
                res.writeHead(404);
                res.end('File not found: ' + req.url);
                return;
            }

            // dir listing (if enabled)
            if (stats.isDirectory()) {
                if(this.index === false) {
                    res.writeHead("403");
                    res.end("Directory listing not allowed.");
                    return;
                }
                serveIndex(filePath, req, res);
                return;
            }

            // stream a file
            var stream = fs.createReadStream(filePath);

            stream.on('error', function(error) {
                try{
                    res.writeHead(500); // This can fail if that has alreday been set
                }catch(e) {}
                res.end("500 error: "+error);
                return;
            });

            var contentType = mime.lookup(path.extname(filePath));
            res.setHeader('Content-Type', contentType);
            res.writeHead(200);

            // note: end is called automatically by pipe
            stream.pipe(res, {});
        });
    }

/**
 * Serve a directory index, with links to contained items.
 */
function serveIndex(folder, req, res) {
    fs.readdir(folder, function(err, files) {
        res.setHeader('Content-Type', "text/html");
        res.writeHead(200);
        res.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\"><html><head><title>Index of ");
        res.write(req.url);
        res.write("</title></head><body><h1>Index of ");
        res.write(req.url);
        res.write("</h1><ul>");
        files.forEach(function(file) {
            var stats = fs.statSync(folder +"/"+ file);
            var path = req.url+file;
            if(stats.isDirectory()) {
                res.write("<li><a href='"+path+ "/'>"+file+"/</a></li>");
            } else {
                res.write("<li><a href='"+path+ "'>"+file+"</a> ("+stats.size+" bytes)</li>");
            }
        });
        res.write("<hr/><address>Powered by Server302 / Node.js</address></body></html>");
        res.end();
    });
}