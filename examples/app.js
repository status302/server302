
var Server302 = require('../lib/server302.js'),
    http = require('http');

var server = new Server302('/tmp/');

http.createServer(function (request, response) {
  server.serve(request, response);
}).listen(8100);

console.log("Started on port 8100")
